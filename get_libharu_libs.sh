#!/bin/bash

if [ -d "libharu" ]; then

	INSTALLDIR="`pwd`"

	mkdir "libraries"
	cd "libharu"
	export DESTDIR="$INSTALLDIR/libraries" && make install
	cd "libraries/usr/local"
	mkdir "static"
	cp "lib/libhpdf.a" "static/"

else

	echo "ERROR: You need to grab libharu's source from github first."
	echo "  EX: git clone https://github.com/libharu/libharu.git"

fi
