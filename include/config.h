// config.h - header: read and create config files

// Copyright (C) 2021  Nathan Taylor
// GPL 3 Only

#ifndef CONFIG_H
#define CONFIG_H


// Structures-------------------------------------------------------------------
typedef struct
{
	char name[64];
	char street[64];
	char city[64];
	char bank[64];
	char bank_city[64];
	char routing_no[10];  // always 9 digits
	char account_no[13];  // up to 12 digits
	char check_no[8];
} Config;

// Structures-------------------------------------------------------------------

// Declarations-----------------------------------------------------------------
Config read_config(FILE * config_file);
void create_config();

// Declarations-----------------------------------------------------------------

#endif
