To Do
=====

- Use config struct instead of individual variables.
- Add fields for writing checks.
- Add interactive mode.
- Update current check number in config (or somewhere else.)
- Record written checks to csv checkbook.
- Add GUI.
