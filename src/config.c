// config.c - read and create config files

// Copyright (C) 2021  Nathan Taylor
// GPL 3 Only

#include <stdio.h>
#include <string.h>
#include "config.h"

// Functions--------------------------------------------------------------------
void store_value(Config * config, char * option, char * value)
{
	if (strncmp(option, "name", 63) == 0)
	{
		strncpy(config->name, value, 63);
		config->name[63] = '\0';
	}
	else if (strncmp(option, "street", 63) == 0)
	{
		strncpy(config->street, value, 63);
		config->street[63] = '\0';
	}
	else if (strncmp(option, "city", 63) == 0)
	{
		strncpy(config->city, value, 63);
		config->city[63] = '\0';
	}
	else if (strncmp(option, "bank", 63) == 0)
	{
		strncpy(config->bank, value, 63);
		config->bank[63] = '\0';
	}
	else if (strncmp(option, "bank_city", 63) == 0)
	{
		strncpy(config->bank_city, value, 63);
		config->bank_city[63] = '\0';
	}
	else if (strncmp(option, "routing_no", 63) == 0)
	{
		strncpy(config->routing_no, value, 9);
		config->routing_no[9] = '\0';
	}
	else if (strncmp(option, "account_no", 63) == 0)
	{
		strncpy(config->account_no, value, 12);
		config->account_no[12] = '\0';
	}
	else if (strncmp(option, "check_no", 63) == 0)
	{
		strncpy(config->check_no, value, 7);
		config->check_no[7] = '\0';
	}
}

Config read_config(FILE * config_file)
{
	Config config;
	char line[256];
	int linenum = 0;

	while (fgets(line, 256, config_file) != NULL)
	{
		char option[64], value[64];
		linenum++;

		// check for comment and empty line
		if (line[0] == '#' || line[0] == '\n')
			continue;

		// check formatting
		if (sscanf(line, "%63s %*1s %63[^\n]", option, value) != 2)
		{
			fprintf(stderr, "Syntax error: line %d\n", linenum);
			continue;
		}

		store_value(&config, option, value);
	}

	return config;
}

void create_config()
{
	return;
}

// Functions--------------------------------------------------------------------

