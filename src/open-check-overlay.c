// open-check-overlay.c - create a check overlay and save it as a pdf

// Copyright (C) 2021  Nathan Taylor

/*  Open Check Overlay is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License only.

    Open Check Overlay is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <setjmp.h>
#include "hpdf.h"
#include "config.h"

const char transit = 'A';
const char onus   = 'C';

// Functions--------------------------------------------------------------------
void print_help()
{
	puts("  \nUsage: open-check-overlay [options]\n");
	puts("  Options:");
	puts("    [-h]         print help");
	puts("    [-v]         print version");
	puts("    [-o outfile] specify output file name");
	puts("");
}

void print_version()
{
	puts("  Open Check Overlay v0.2");
}

// handle errors
void error_handler(HPDF_STATUS error_no, HPDF_STATUS detail_no, void * env)
{
	printf("ERROR: error_no=%04X, detail_no=%d\n",
	       (unsigned int) error_no,
	       (int) detail_no);
	longjmp(env, 1); // invoke longjmp() on error
}

// create the pdf
int create_pdf(
               const char * name,
               const char * street,
               const char * city,
               const char * bank,
               const char * bank_city,
               const char * routing_no,
               const char * account_no,
               const char * check_no,
               const char * output_filename
              )
{
	// create pdf object
	HPDF_Doc pdf;
	jmp_buf env;

	pdf = HPDF_New(error_handler, env);

	// handle errors
	if (!pdf)
	{
		printf("ERROR: cannot create pdf object.\n");
		return 1;
	}

	if (setjmp(env))
	{
		HPDF_Free(pdf);
		return 1;
	}

	// create pdf page
	HPDF_Page page_1;

	page_1 = HPDF_AddPage(pdf);
	HPDF_Page_SetSize(page_1, HPDF_PAGE_SIZE_LETTER, HPDF_PAGE_PORTRAIT);

	// drawing operations-------------------------------------------------------
	// fonts
	HPDF_Font font      = HPDF_GetFont(pdf, "Helvetica", "StandardEncoding");
	HPDF_Font font_bold = HPDF_GetFont(pdf, "Helvetica-Bold", "StandardEncoding");

	// company address text
	HPDF_Page_BeginText(page_1);

	HPDF_Page_SetFontAndSize(page_1, font_bold, 10);
	HPDF_Page_MoveTextPos(page_1, 47, 762);
	HPDF_Page_ShowText(page_1, name);

	HPDF_Page_SetFontAndSize(page_1, font, 10);
	HPDF_Page_MoveTextPos(page_1, 0, -12);  // relative
	HPDF_Page_ShowText(page_1, street);

	HPDF_Page_MoveTextPos(page_1, 0, -12);  // relative
	HPDF_Page_ShowText(page_1, city);

	HPDF_Page_SetFontAndSize(page_1, font_bold, 10);
	HPDF_Page_MoveTextPos(page_1, 9, -216);
	HPDF_Page_ShowText(page_1, name);

	HPDF_Page_MoveTextPos(page_1, 0, -251);
	HPDF_Page_ShowText(page_1, name);

	HPDF_Page_EndText(page_1);
	
	// bank address text
	HPDF_Page_BeginText(page_1);

	HPDF_Page_SetFontAndSize(page_1, font_bold, 10);
	HPDF_Page_MoveTextPos(page_1, 339, 761);
	HPDF_Page_ShowText(page_1, bank);

	HPDF_Page_SetFontAndSize(page_1, font, 10);
	HPDF_Page_MoveTextPos(page_1, 0, -12);  // relative
	HPDF_Page_ShowText(page_1, bank_city);

	HPDF_Page_EndText(page_1);

	// check number text
	HPDF_Page_BeginText(page_1);

	HPDF_Page_SetFontAndSize(page_1, font, 12);
	HPDF_Page_MoveTextPos(page_1, 538, 754);
	HPDF_Page_ShowText(page_1, check_no);

	HPDF_Page_MoveTextPos(page_1, 0, -238);
	HPDF_Page_ShowText(page_1, check_no);

	HPDF_Page_MoveTextPos(page_1, 0, -251);
	HPDF_Page_ShowText(page_1, check_no);

	HPDF_Page_EndText(page_1);

	// body text
	HPDF_Page_BeginText(page_1);

	HPDF_Page_SetFontAndSize(page_1, font, 8);
	HPDF_Page_MoveTextPos(page_1, 25, 697);
	HPDF_Page_ShowText(page_1, "PAY TO THE");

	HPDF_Page_MoveTextPos(page_1, 0, -10);
	HPDF_Page_ShowText(page_1, "ORDER OF");

	HPDF_Page_MoveTextPos(page_1, 0, -100);
	HPDF_Page_ShowText(page_1, "MEMO");

	HPDF_Page_SetFontAndSize(page_1, font_bold, 7);
	HPDF_Page_MoveTextPos(page_1, 430, -2);
	HPDF_Page_ShowText(page_1, "VOID AFTER 90 DAYS");

	HPDF_Page_SetFontAndSize(page_1, font, 8);
	HPDF_Page_MoveTextPos(page_1, 98, 76);
	HPDF_Page_ShowText(page_1, "DOLLARS");

	HPDF_Page_SetFontAndSize(page_1, font, 10);
	HPDF_Page_MoveTextPos(page_1, -61, 25);
	HPDF_Page_ShowText(page_1, "$");

	HPDF_Page_EndText(page_1);

	// load micr font
	const char * micr_font_name = HPDF_LoadTTFontFromFile(pdf, "fonts/GnuMICR.ttf", HPDF_TRUE);
	HPDF_Font micr_font = HPDF_GetFont(pdf, micr_font_name, "StandardEncoding");

	// create micr string
	char micr_line[64] = "";

	micr_line[0] = onus;
	strncat(micr_line, check_no, 6);
	micr_line[7] = onus;

	micr_line[8] = ' ';

	micr_line[9] = transit;
	strncat(micr_line, routing_no, 9);
	micr_line[19] = transit;

	strcat(micr_line, account_no);
	int len = (int) strlen(micr_line);
	micr_line[len] = onus;

	// account number text
	HPDF_Page_BeginText(page_1);

	HPDF_Page_SetFontAndSize(page_1, micr_font, 12);
	HPDF_Page_MoveTextPos(page_1, 122, 555);
	HPDF_Page_ShowText(page_1, micr_line);

	HPDF_Page_EndText(page_1);

	// lines
	HPDF_Page_SetLineWidth(page_1, 0.5);

	// date line
	HPDF_Page_MoveTo(page_1, 499, 720);
	HPDF_Page_LineTo(page_1, 589, 720);
	HPDF_Page_Stroke(page_1);

	// pay to line
	HPDF_Page_MoveTo(page_1, 62, 684);
	HPDF_Page_LineTo(page_1, 490, 684);
	HPDF_Page_Stroke(page_1);

	// amount number line
	HPDF_Page_MoveTo(page_1, 499, 684);
	HPDF_Page_LineTo(page_1, 589, 684);
	HPDF_Page_Stroke(page_1);

	// amount text line
	HPDF_Page_MoveTo(page_1, 19, 661);
	HPDF_Page_LineTo(page_1, 549, 661);
	HPDF_Page_Stroke(page_1);

	// drawing operations-------------------------------------------------------

	// save pdf to file
	HPDF_SaveToFile(pdf, output_filename);

	// cleanup
	HPDF_Free(pdf);

	return 0;
}

// Functions--------------------------------------------------------------------

// Execution--------------------------------------------------------------------
int main(int argc, char * argv[])
{
	// input variables
	int arg;
	Config config;
	char output_filename[PATH_MAX] = "output.pdf";

	// loop through args
	while ((arg = getopt(argc, argv, ":hvn:s:c:b:m:r:a:k:o:")) != -1)
	{
		switch(arg)
		{
			// help
			case 'h':
				print_help();
				return 0;
			// version
			case 'v':
				print_version();
				return 0;
			// name
			case 'n':
				strncpy(config.name, optarg, sizeof(config.name) - 1);
				break;
			// street
			case 's':
				strncpy(config.street, optarg, sizeof(config.street) - 1);
				break;
			// city
			case 'c':
				strncpy(config.city, optarg, sizeof(config.city) - 1);
				break;
			// bank
			case 'b':
				strncpy(config.bank, optarg, sizeof(config.bank) - 1);
				break;
			// bank city
			case 'm':
				strncpy(config.bank_city, optarg, sizeof(config.bank_city) - 1);
				break;
			// routing number
			case 'r':
				strncpy(config.routing_no, optarg, sizeof(config.routing_no) - 1);
				break;
			// account number
			case 'a':
				strncpy(config.account_no, optarg, sizeof(config.account_no) - 1);
				break;
			case 'k':
				strncpy(config.check_no, optarg, sizeof(config.check_no) - 1);
				break;
			// output file
			case 'o':
				strncpy(output_filename, optarg, sizeof(output_filename) - 1);
				break;
			// missing value
			case ':':
				printf("  Missing a value for option: \"%c\"\n", optopt);
				return 0;
			// unknown option
			case '?':
				printf("  Unknown option: \"%c\"\n", optopt);
				return 0;
		}
	}

	// start interactive mode, if no args
	if (argc == 1)
	{
		print_version();
		puts("  Starting interactive mode.");

		// test config reading TODO: move to beginning of main
		FILE * config_file;
		config_file = fopen("checks.conf", "r");
		if (config_file != NULL)
		{
			config = read_config(config_file);
			fclose(config_file);
		}

		/* interactive mode test
		while (1)
		{
			puts("  Press Enter to exit...");
			printf("  (checks) ");
			if (getchar() == '\n')
			{
				return 0;
			}
		}
		*/
	}

	// create the pdf
	int result = create_pdf(
	                        config.name,
	                        config.street,
	                        config.city,
	                        config.bank,
	                        config.bank_city,
	                        config.routing_no,
	                        config.account_no,
	                        config.check_no,
	                        output_filename
	                       );

	return result;
}

// Execution--------------------------------------------------------------------

